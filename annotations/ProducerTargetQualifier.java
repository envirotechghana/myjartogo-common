package com.envirotech.myjartogo.cdi.annotations;

import javax.enterprise.util.AnnotationLiteral;

public class ProducerTargetQualifier extends AnnotationLiteral<ProducerTarget> 
	implements ProducerTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7327720126711642411L;
}