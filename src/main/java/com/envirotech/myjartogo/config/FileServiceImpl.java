package com.envirotech.myjartogo.config;

import java.io.File;

public class FileServiceImpl implements FileService {

	@Override
	public File loadFile(String filename) {
		return new File(filename);
	}

}
