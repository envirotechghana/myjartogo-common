package com.envirotech.myjartogo.config;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.quakearts.appbase.Main;
import com.quakearts.appbase.internal.properties.ConfigurationPropertyMap;

@Singleton
public class Environment {
	private ConfigurationPropertyMap configurationMap;
	@Inject
	private FileService fileService;
	
	private ConfigurationPropertyMap getConfigurationMap() {
		if(configurationMap == null) {
			File configFile = fileService.loadFile("etc"+File.separator+"jartogo.config.json");
			if(configFile.exists())
				configurationMap = Main.getAppBasePropertiesLoader().loadParametersFromFile(configFile);
			else
				configurationMap = Main.getAppBasePropertiesLoader().loadParametersFromEnvironment("JARTOGO");
		}		
		return configurationMap;
	}

	public int getInt(String key, int defaultValue) {
		if(getConfigurationMap().containsKey(key)) {
			return getConfigurationMap().getInt(key);
		}
		return defaultValue;
	}
	
	public String get(String key, String defaultValue) {
		if(getConfigurationMap().containsKey(key)) {
			return getConfigurationMap().getString(key);
		}
		return defaultValue;
	}
}
