package com.envirotech.myjartogo.config;

import java.io.File;

public interface FileService {
	File loadFile(String name);
}
