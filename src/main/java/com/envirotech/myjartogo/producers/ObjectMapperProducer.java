package com.envirotech.myjartogo.producers;

import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class ObjectMapperProducer {

	private static ObjectMapper objectMapper;

	private ObjectMapperProducer() {}
	
	@Produces
	public static ObjectMapper getObjectMapper() {
		if(objectMapper == null){
			objectMapper = new ObjectMapper();
			objectMapper
				.setSerializationInclusion(Include.NON_EMPTY)
				.registerModule(new JavaTimeModule());
		}
		return objectMapper;
	}
}
