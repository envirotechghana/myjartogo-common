package com.envirotech.myjartogo.httpclient;

public interface HttpCodedResponse {
	void setHttpCode(int httpCode);
	int getHttpCode();
}
