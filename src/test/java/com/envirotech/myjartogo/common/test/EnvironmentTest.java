package com.envirotech.myjartogo.common.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import static org.hamcrest.core.Is.*;

import javax.inject.Inject;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.envirotech.myjartogo.common.test.alternatives.AlternativeFileService;
import com.envirotech.myjartogo.common.test.rules.SystemEnvironmentRule;
import com.envirotech.myjartogo.config.Environment;
import com.quakearts.webtools.test.CDIRunner;

@RunWith(CDIRunner.class)
public class EnvironmentTest {

	@Inject
	private Environment environment;
	
	@Rule
	public SystemEnvironmentRule systemEnvironmentRule = SystemEnvironmentRule.instance();
	
	@Test
	public void testUsingFile() throws Exception {
		clearMap();
		systemEnvironmentRule.clearAll();
		assertThat(environment.get("test.value", ""), is("test value"));
		assertThat(environment.get("not.there", "not there"), is("not there"));
		assertThat(environment.getInt("test.int", 0), is(2));
		assertThat(environment.getInt("not.there.int", -1), is(-1));
		systemEnvironmentRule.reset();
	}

	@Test
	public void testUsingEnvironment() throws Exception {
		clearMap();
		AlternativeFileService.returnNonExistentFile();
		systemEnvironmentRule.set("JARTOGO_TEST__VALUE").as("environment test value")
		.set("JARTOGO_TEST__INT").as(":8");
		assertThat(environment.get("test.value", ""), is("environment test value"));
		assertThat(environment.get("not.there", "environment not there"), is("environment not there"));
		assertThat(environment.getInt("test.int", 0), is(8));
		assertThat(environment.getInt("not.there.int", -4), is(-4));
	}

	private void clearMap() throws Exception {
		Field configurationMapField = Environment.class.getDeclaredField("configurationMap");
		configurationMapField.setAccessible(true);
		configurationMapField.set(environment, null);
	}

}
