package com.envirotech.myjartogo.common.test.alternatives;

import java.io.File;

import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.interceptor.Interceptor;

import com.envirotech.myjartogo.config.FileServiceImpl;

@Alternative
@Priority(Interceptor.Priority.APPLICATION)
public class AlternativeFileService extends FileServiceImpl {

	private static boolean returnNonExistentFile;

	@Override
	public File loadFile(String filename) {
		if(returnNonExistentFile){
			returnNonExistentFile = false;
			return new File("does.not.exist.config.json");
		}
		return super.loadFile(filename);
	}

	public static void returnNonExistentFile() {
		returnNonExistentFile = true;
	}
}
