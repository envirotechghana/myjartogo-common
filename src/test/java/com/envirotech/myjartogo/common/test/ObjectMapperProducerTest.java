package com.envirotech.myjartogo.common.test;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quakearts.webtools.test.CDIRunner;

@RunWith(CDIRunner.class)
public class ObjectMapperProducerTest {

	@Inject
	private ObjectMapper objectMapper;

	@Test
	public void testGetObjectMapper() throws Exception {
		assertNotNull(objectMapper);
		TestBean bean = new TestBean();
		bean.setDate(LocalDate.now());
		bean.setDateTime(LocalDateTime.now());
		
		String beanString = objectMapper.writeValueAsString(bean);
		TestBean bean2 = objectMapper.readValue(beanString, TestBean.class);
		assertThat(bean2, is(bean));
		assertThat(bean2.hashCode(), is(bean.hashCode()));
		System.out.println(beanString);
		assertThat(objectMapper.writeValueAsString(new TestBean()), is("{}"));
	}

	@JsonPropertyOrder({
		"dateTime",
		"date"
	})
	
	public static class TestBean {
		private LocalDateTime dateTime;
		private LocalDate date;

		public LocalDateTime getDateTime() {
			return dateTime;
		}

		public void setDateTime(LocalDateTime dateTime) {
			this.dateTime = dateTime;
		}

		public LocalDate getDate() {
			return date;
		}

		public void setDate(LocalDate date) {
			this.date = date;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + date.hashCode();
			result = prime * result + dateTime.hashCode();
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			TestBean other = (TestBean) obj;
			if (!date.equals(other.date))
				return false;
			if(!dateTime.equals(other.dateTime))
				return false;
			return true;
		}		
	}
}
